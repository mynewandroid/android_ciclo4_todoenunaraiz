import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: "Hola Mundo",
      home: EjercicioPage(),
    );
  }
}

class EjercicioPage extends StatelessWidget {
  const EjercicioPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 60,
            ),
            Image.network(
                "https://concepto.de/wp-content/uploads/2015/03/paisaje-800x409.jpg"),
            Align(
              alignment: const AlignmentDirectional(0, 0.05),
              child: Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(30, 20, 40, 20),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: const [
                          Align(
                            alignment: AlignmentDirectional(-1, 0),
                            child: Text(
                              'Lago Calima.',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                          ),
                          Align(
                            alignment: AlignmentDirectional(-1, 0),
                            child: Text(
                              'Valle del Cauca, Colombia',
                              textAlign: TextAlign.start,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Icon(
                      Icons.star_rate,
                      color: Color(0xFFFF0000),
                      size: 24,
                    ),
                    const Align(
                      alignment: AlignmentDirectional(0, 0),
                      child: Text(
                        '41',
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(0, 15, 0, 20),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    children: const [
                      Icon(
                        Icons.call,
                        color: Color(0xFF3B8BD0),
                        size: 24,
                      ),
                      Text(
                        'Llamar',
                        style: TextStyle(
                          color: Color(0xFF3B8BD0),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    children: const [
                      Icon(
                        Icons.location_pin,
                        color: Color(0xFF3B8BD0),
                        size: 24,
                      ),
                      Text(
                        'Ruta',
                        style: TextStyle(
                          color: Color(0xFF3B8BD0),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    children: const [
                      Icon(
                        Icons.share_rounded,
                        color: Color(0xFF3B8BD0),
                        size: 24,
                      ),
                      Text(
                        'Compartir',
                        style: TextStyle(
                          color: Color(0xFF3B8BD0),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
              child: Text(
                'El Lago Calima es un reservorio de agua artificial con características interesantes, también es uno de los embalses más grandes de América. Este lugar cuenta con un atractivo turístico especial que está enmarcado con hermosas formaciones montañosas, cuyo encanto atrae a miles de turistas cada año.\n\nEl Lago Calima se ubica en el municipio de Calima el Darién en el Departamento del Valle del Cauca en Colombia, en el continente suramericano, este reconocido lago es visitado por cientos de turistas de diferentes nacionalidades cada día y todos quedan fascinados por la belleza de sus verdes paisajes.',
                textAlign: TextAlign.start,
                maxLines: 30,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
